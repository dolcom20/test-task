package com.example.application.ui.views.recipes;

import com.example.application.backend.entity.Doctor;
import com.example.application.backend.entity.Patient;
import com.example.application.backend.entity.Recipe;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

import java.util.List;

public class RecipeForm extends FormLayout {
    private Recipe recipe;

    TextField description = new TextField("Description");
    ComboBox<Patient> patient = new ComboBox<>("Patient");
    ComboBox<Doctor> doctor = new ComboBox<>("Doctor");
//    DatePicker createdOn = new DatePicker("Date of creation");
    ComboBox<Recipe.Term> term = new ComboBox<>("Term");
    ComboBox<Recipe.Status> status = new ComboBox<>("Status");

    Button okButton = new Button("OK");
    Button cancelButton = new Button("Отмена");

//    Binder<Recipe> binder = new Binder<>(Recipe.class);

    Binder<Recipe> binder = new Binder<>(Recipe.class);

    public RecipeForm(List<Patient> patients, List<Doctor> doctors) {
        addClassName("recipe-form");

        binder.bindInstanceFields(this);

//        binder.bind(description,
//                recipe->recipe.getDescription(),
//                (recipe, description)-> recipe.setDescription(description)
//        );

        term.setItems(Recipe.Term.values());
        status.setItems(Recipe.Status.values());

        patient.setItems(patients);
        doctor.setItems(doctors);
        add(description, patient, doctor, term, status, getControls());
    }

    private Component getControls() {
        okButton.addClickListener(evt -> fireEvent(new SaveEvent(this, binder.getBean())));
        cancelButton.addClickListener(evt -> fireEvent(new CloseEvent(this)));
        return new HorizontalLayout(okButton, cancelButton);
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    // Events
    public static abstract class RecipeFormEvent extends ComponentEvent<RecipeForm> {
        private Recipe recipe;

        protected RecipeFormEvent(RecipeForm source, Recipe recipe) {
            super(source, false);
            this.recipe = recipe;
        }

        public Recipe getRecipe() {
            return recipe;
        }
    }

    public static class SaveEvent extends RecipeForm.RecipeFormEvent {
        SaveEvent(RecipeForm source, Recipe recipe) {
            super(source, recipe);
        }
    }

    public static class CloseEvent extends RecipeForm.RecipeFormEvent {
        CloseEvent(RecipeForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
