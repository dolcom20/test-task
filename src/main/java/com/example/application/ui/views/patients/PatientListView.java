package com.example.application.ui.views.patients;

import com.example.application.backend.entity.Patient;
import com.example.application.backend.service.PatientService;
import com.example.application.ui.views.MainLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "", layout = MainLayout.class)
@PageTitle("Patients")
@CssImport("styles/views/patients/patient-list-view.css")
public class PatientListView extends VerticalLayout {
    private Patient patient;

    private final PatientService patientService;
    private final PatientForm form = new PatientForm();
    Grid<Patient> grid = new Grid<>(Patient.class);

    Button addButton = new Button("Добавить");
    Button editButton = new Button("Изменить");
    Button deleteButton = new Button("Удалить");

    public PatientListView(PatientService patientService) {
        this.patientService = patientService;

        addClassName("list-view");
        setSizeFull();

        configureGrid();

        Div content = new Div(grid, form);

        form.addListener(PatientForm.SaveEvent.class, this::savePatient);
        form.addListener(PatientForm.CloseEvent.class, evt -> closeEditor());

        content.addClassName("content");
        content.setSizeFull();

        add(getControls(), content);

        updateList();
        closeEditor();
    }

    private void savePatient(PatientForm.SaveEvent evt) {
        patientService.save(evt.getPatient());
        updateList();
        closeEditor();
    }

    private void closeEditor() {
        form.setVisible(false);
        setCurrent(null);
        removeClassName("editing");
    }

    private void setCurrent(Patient patient) {
        this.patient = patient;
        form.setPatient(patient);

        if (patient != null && patient.getId() != null) {
            editButton.setEnabled(true);
            deleteButton.setEnabled(true);
        } else {
            editButton.setEnabled(false);
            deleteButton.setEnabled(false);
        }
    }

    private void configureGrid() {
        grid.addClassName("patient-grid");
        grid.setSizeFull();
        grid.setColumns("surname", "name", "middleName", "phone");
        grid.asSingleSelect().addValueChangeListener(evt -> setCurrent(evt.getValue()));
    }

    private HorizontalLayout getControls() {
        addButton.addClickListener(evt -> addPatient());
        editButton.addClickListener(evt -> openEditor());
        deleteButton.addClickListener(evt -> deletePatient());

        addButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        editButton.setEnabled(false);
        deleteButton.setEnabled(false);

        return new HorizontalLayout(addButton, editButton, deleteButton);
    }

    private void addPatient() {
        setCurrent(new Patient());
        openEditor();
    }

    private void deletePatient() {
        patientService.delete(this.patient);
        updateList();
    }

    private void openEditor() {
        form.setVisible(true);
    }

    private void updateList() {
        grid.setItems(this.patientService.findAll());
    }

}
