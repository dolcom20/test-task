package com.example.application.ui.views.doctors;

import com.example.application.backend.entity.Doctor;
import com.example.application.ui.views.patients.PatientForm;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.data.binder.Binder;

public class DoctorForm extends FormLayout {
    private Doctor doctor;

    TextField surname = new TextField("Surname");
    TextField name = new TextField("Name");
    TextField middleName = new TextField("Middle name");
    ComboBox<Doctor.Speciality> speciality = new ComboBox<>("Speciality");

    Button okButton = new Button("OK");
    Button cancelButton = new Button("Cancel");

    Binder<Doctor> binder = new Binder<>(Doctor.class);

    public DoctorForm() {
        addClassName("doctor-form");

        speciality.setItems(Doctor.Speciality.values());

        binder.bindInstanceFields(this);

        add(
            surname,
            name,
            middleName,
            speciality,
            getControls()
        );
    }

    private Component getControls() {
        okButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        okButton.addClickListener(evt -> fireEvent(new DoctorForm.SaveEvent(this, binder.getBean())));
        cancelButton.addClickListener(evt -> fireEvent(new DoctorForm.CloseEvent(this)));

        return new HorizontalLayout(okButton, cancelButton);
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
        binder.setBean(doctor);
    }

    // Events
    public static abstract class DoctorFormEvent extends ComponentEvent<DoctorForm> {
        private Doctor doctor;

        protected DoctorFormEvent(DoctorForm source, Doctor doctor) {
            super(source, false);
            this.doctor = doctor;
        }

        public Doctor getDoctor() {
            return doctor;
        }
    }

    public static class SaveEvent extends DoctorForm.DoctorFormEvent {
        SaveEvent(DoctorForm source, Doctor doctor) {
            super(source, doctor);
        }
    }

    public static class CloseEvent extends DoctorForm.DoctorFormEvent {
        CloseEvent(DoctorForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
