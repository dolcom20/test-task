package com.example.application.ui.views.patients;

import com.example.application.backend.entity.Patient;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

public class PatientForm extends FormLayout {
    private Patient patient;

    TextField surname = new TextField("Surname");
    TextField name = new TextField("Name");
    TextField middleName = new TextField("Middle name");
    TextField phone = new TextField("Phone");

    Button okButton = new Button("OK");
    Button cancelButton = new Button("Cancel");

    Binder<Patient> binder = new Binder<>(Patient.class);

    public PatientForm() {
        addClassName("patient-form");

        binder.bindInstanceFields(this);

        add(
            surname,
            name,
            middleName,
            phone,
            getControls()
        );
    }

    private Component getControls() {
        okButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        okButton.addClickListener(evt -> fireEvent(new SaveEvent(this, binder.getBean())));
        cancelButton.addClickListener(evt -> fireEvent(new CloseEvent(this)));

        return new HorizontalLayout(okButton, cancelButton);
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
        binder.setBean(patient);
    }

    // Events
    public static abstract class PatientFormEvent extends ComponentEvent<PatientForm> {
        private Patient patient;

        protected PatientFormEvent(PatientForm source, Patient patient) {
            super(source, false);
            this.patient = patient;
        }

        public Patient getPatient() {
            return patient;
        }
    }

    public static class SaveEvent extends PatientFormEvent {
        SaveEvent(PatientForm source, Patient patient) {
            super(source, patient);
        }
    }

    public static class CloseEvent extends PatientFormEvent {
        CloseEvent(PatientForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
