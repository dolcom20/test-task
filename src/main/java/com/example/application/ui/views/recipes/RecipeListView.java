package com.example.application.ui.views.recipes;

import com.example.application.backend.entity.Doctor;
import com.example.application.backend.entity.Patient;
import com.example.application.backend.entity.Recipe;
import com.example.application.backend.service.DoctorService;
import com.example.application.backend.service.PatientService;
import com.example.application.backend.service.RecipeService;
import com.example.application.ui.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "recipes", layout = MainLayout.class)
@PageTitle("Recipes")
@CssImport("styles/views/recipes/recipe-list-view.css")
public class RecipeListView extends VerticalLayout {
    private final RecipeService recipeService;
    private final PatientService patientService;
    private final DoctorService doctorService;
    private final RecipeForm form;

    Button addButton = new Button("Add recipe");
    Button editButton = new Button("Edit");
    Button deleteButton = new Button("Delete");

    TextField descriptionFilter = new TextField("Description");
    ComboBox<Patient> patientFilter = new ComboBox<>("Patient");
    ComboBox<Recipe.Status> statusFilter = new ComboBox<>("Status");

    Grid<Recipe> grid = new Grid<>(Recipe.class);
    private Recipe recipe;

    public RecipeListView(RecipeService recipeService,
                          PatientService patientService,
                          DoctorService doctorService) {
        this.recipeService = recipeService;
        this.patientService = patientService;
        this.doctorService = doctorService;

        addClassName("list-view");
        setSizeFull();

        configureGrid();

        form = new RecipeForm(patientService.findAll(), doctorService.findAll());

        form.addListener(RecipeForm.SaveEvent.class, this::saveRecipe);
        form.addListener(RecipeForm.CloseEvent.class, evt -> closeEditor());

        Div content = new Div(grid, form);
        content.addClassName("content");
        content.setSizeFull();

        add(getControls(), getFilter(), content);
        closeEditor();
    }

    private void configureGrid() {
        grid.addClassName("recipe-grid");
        grid.setSizeFull();

        grid.removeColumnByKey("patient");
        grid.removeColumnByKey("doctor");

        grid.setColumns("description", "term", "status");

        grid.addColumn(recipe -> {
            Patient patient = recipe.getPatient();
            return patient == null ? "-" : patient;
        }).setHeader("Patient");

        grid.addColumn(recipe -> {
            Doctor doctor = recipe.getDoctor();
            return doctor == null ? "-" : doctor;
        }).setHeader("Doctor");

        grid.asSingleSelect().addValueChangeListener(evt -> setCurrent(evt.getValue()));
    }

    private void saveRecipe(RecipeForm.SaveEvent evt) {
        if (evt.getRecipe() != null) {
            recipeService.save(evt.getRecipe());
            updateList();
            closeEditor();
        } else {
            System.out.println("bean is null");
        }
    }

    private void setCurrent(Recipe recipe) {
        this.recipe = recipe;
    }

    private void closeEditor() {
        form.setVisible(false);
    }

    private Component getControls() {
        addButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        addButton.addClickListener(evt -> editRecipe(new Recipe()));
        deleteButton.addClickListener(evt -> deleteRecipe());

        return new HorizontalLayout(addButton, editButton, deleteButton);
    }

    private void editRecipe(Recipe recipe) {
        if (recipe == null) {
            closeEditor();
        } else {
            form.setRecipe(recipe);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void deleteRecipe() {
        recipeService.delete(recipe);
    }

    private HorizontalLayout getFilter() {
        descriptionFilter.addValueChangeListener(evt ->
                grid.setItems(recipeService.findAll(evt.getValue())));
        patientFilter.setItems(patientService.findAll());
        statusFilter.setItems(Recipe.Status.values());
        return new HorizontalLayout(descriptionFilter, patientFilter, statusFilter);
    }

    private void updateList() {
        grid.setItems(recipeService.findAll(descriptionFilter.getValue()));
    }
}
