package com.example.application.ui.views.doctors;

import com.example.application.backend.entity.Doctor;
import com.example.application.backend.service.DoctorService;
import com.example.application.ui.views.MainLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@Route(value = "doctors", layout = MainLayout.class)
@PageTitle("Doctors")
@CssImport("styles/views/doctors/doctor-list-view.css")
public class DoctorListView extends VerticalLayout {
    private Doctor doctor;

    private final DoctorService doctorService;
    private final DoctorForm form = new DoctorForm();
    Grid<Doctor> grid = new Grid<>(Doctor.class);

    Button addButton = new Button("Добавить");
    Button editButton = new Button("Изменить");
    Button deleteButton = new Button("Удалить");

    public DoctorListView(DoctorService doctorService) {
        this.doctorService = doctorService;

        addClassName("list-view");
        setSizeFull();

        configureGrid();

        Div content = new Div(grid, form);

        form.addListener(DoctorForm.SaveEvent.class, this::saveDoctor);
        form.addListener(DoctorForm.CloseEvent.class, evt -> closeEditor());

        content.addClassName("content");
        content.setSizeFull();

        add(getControls(), content);

        updateList();
        closeEditor();
    }

    private void configureGrid() {
        grid.addClassName("doctor-grid");
        grid.setSizeFull();
        grid.setColumns("surname", "name", "middleName");
        grid.addColumn(doctor -> {
            return doctor.getSpeciality().getName();
        }).setHeader("Speciality");
        grid.asSingleSelect().addValueChangeListener(evt -> setCurrent(evt.getValue()));
    }

    private HorizontalLayout getControls() {
        addButton.addClickListener(evt -> addDoctor());
        editButton.addClickListener(evt -> openEditor());
        deleteButton.addClickListener(evt -> deleteDoctor());

        addButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        editButton.setEnabled(false);
        deleteButton.setEnabled(false);

        return new HorizontalLayout(addButton, editButton, deleteButton);
    }

    private void addDoctor() {
        setCurrent(new Doctor());
        openEditor();
    }

    private void setCurrent(Doctor doctor) {
        this.doctor = doctor;
        form.setDoctor(doctor);

        if (doctor != null && doctor.getId() != null) {
            editButton.setEnabled(true);
            deleteButton.setEnabled(true);
        } else {
            editButton.setEnabled(false);
            deleteButton.setEnabled(false);
        }
    }

    private void openEditor() {
        form.setVisible(true);
    }

    private void closeEditor() {
        form.setVisible(false);
        setCurrent(null);
        removeClassName("editing");
    }

    private void deleteDoctor() {
        doctorService.delete(this.doctor);
        updateList();
    }

    private void updateList() {
        grid.setItems(doctorService.findAll());
    }

    private void saveDoctor(DoctorForm.SaveEvent evt) {
        doctorService.save(evt.getDoctor());
        updateList();
        closeEditor();
    }
}
