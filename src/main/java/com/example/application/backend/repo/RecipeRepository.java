package com.example.application.backend.repo;

import com.example.application.backend.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    @Query("select r from Recipe r " +
            "where lower(r.description) like lower(concat('%', :searchVal, '%'))")
    List<Recipe> search(@Param("searchVal") String searchVal);
}
