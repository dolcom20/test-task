package com.example.application.backend.service;

import com.example.application.backend.entity.Patient;
import com.example.application.backend.repo.PatientRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<Patient> findAll() {
        return this.patientRepository.findAll();
    }

    public void delete(Patient patient) {
        patientRepository.delete(patient);
    }

    @PostConstruct
    public void populateTestData() {
        if (patientRepository.count() == 0) {
            patientRepository.saveAll(
                Stream.of("Иванов Иван Иваныч 89271234567", "Петров Пётр Петрович 89061234567",
                        "Сидоров Сидор Сидорыч 89601234567")
                        .map(data -> {
                            String[] split = data.split(" ");

                            Patient patient = new Patient();

                            patient.setSurname(split[0]);
                            patient.setName(split[1]);
                            patient.setMiddleName(split[2]);
                            patient.setPhone(split[3]);

                            return patient;
                        }).collect(Collectors.toList())
            );
        }
    }

    public void save(Patient patient) {
        patientRepository.save(patient);
    }
}
