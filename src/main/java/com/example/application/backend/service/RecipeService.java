package com.example.application.backend.service;

import com.example.application.backend.entity.Recipe;
import com.example.application.backend.repo.RecipeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecipeService {

    private final RecipeRepository recipeRepository;

    public RecipeService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public List<Recipe> findAll() {
        return this.recipeRepository.findAll();
    }

    public List<Recipe> findAll(String searchVal) {
        if (searchVal == null || searchVal.isEmpty())
            return this.findAll();
        else
            return this.recipeRepository.search(searchVal);
    }

    public void save(Recipe recipe) {
        this.recipeRepository.save(recipe);
    }

    public void delete(Recipe recipe) {
        this.recipeRepository.delete(recipe);
    }


}
