package com.example.application.backend.service;

import com.example.application.backend.entity.Doctor;
import com.example.application.backend.repo.DoctorRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DoctorService<T> {

    private final DoctorRepository doctorRepository;

    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public List<Doctor> findAll() {
        return this.doctorRepository.findAll();
    }

    public void delete(Doctor doctor) {
        doctorRepository.delete(doctor);
    }

    @PostConstruct
    public void populateTestData() {
        if (doctorRepository.count() == 0) {
            doctorRepository.saveAll(
                    Stream.of("Алексеев Пётр Алексеевич SURGEON", "Коновалов Сергей Фёдорович THERAPIST",
                            "Курпатова Ольга Михайловна OTOLARYNGOLOGIST")
                            .map(data -> {
                                Doctor doctor = new Doctor();

                                String[] split = data.split(" ");

                                doctor.setSurname(split[0]);
                                doctor.setName(split[1]);
                                doctor.setMiddleName(split[2]);
                                doctor.setSpeciality(Doctor.Speciality.valueOf(split[3]));

                                return doctor;
                            }).collect(Collectors.toList())
            );
        }
    }

    public void save(Doctor doctor) {
        doctorRepository.save(doctor);
    }
}
