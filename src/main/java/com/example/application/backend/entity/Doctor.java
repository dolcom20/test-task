package com.example.application.backend.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Doctor extends AbstractEntity {
    public enum Speciality {
        SURGEON("Хирург"), THERAPIST("Терапевт"), OTOLARYNGOLOGIST("Отоларинголог");

        private String name;

        Speciality(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @NotNull
    @NotEmpty
    private String surname = "";

    @NotNull
    @NotEmpty
    private String name = "";

    @NotNull
    @NotEmpty
    private String middleName = "";

    @NotNull
    @NotEmpty
    private Doctor.Speciality speciality;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    @Override
    public String toString() {
        return surname + " " + name + " " + middleName;
    }
}
