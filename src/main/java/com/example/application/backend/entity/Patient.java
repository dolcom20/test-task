package com.example.application.backend.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Patient extends AbstractEntity implements Cloneable {

    @NotNull
    @NotEmpty
    private String surname = "";

    @NotNull
    @NotEmpty
    private String name = "";

    @NotNull
    @NotEmpty
    private String middleName = "";

    @NotNull
    @NotEmpty
    private String phone = "";

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return surname + " " + name + " " + middleName;
    }
}
