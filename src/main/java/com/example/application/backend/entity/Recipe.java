package com.example.application.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Recipe extends AbstractEntity implements Cloneable {

    public enum Term {
        ONE_MONTH("1 месяц"),
        THREE_MONTHS("3 месяца"),
        SIX_MONTHS("6 месяцев"),
        ONE_YEAR("12 месяцев");

        private String name;

        Term(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }


        @Override
        public String toString() {
            return name;
        }
    }

    public enum Status {
        NORMAL("Нормальный"),
        CITO("Срочный"),
        STATIM("Немедленный");

        private String name;

        Status(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }


        @Override
        public String toString() {
            return name;
        }
    }

    @NotNull
    @NotEmpty
    private String description = "";

    @OneToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @OneToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

//    @NotNull
//    private LocalDate createdOn;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Term term;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

//    public LocalDate getCreatedOn() {
//        return createdOn;
//    }
//
//    public void setCreatedOn(LocalDate createdOn) {
//        this.createdOn = createdOn;
//    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return description;
    }
}
